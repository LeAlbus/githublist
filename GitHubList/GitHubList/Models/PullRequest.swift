//
//  PullRequest.swift
//  GitHubList
//
//  Created by Pedro Lopes on 14/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation

struct PullRequest: Decodable{
    
    let url: String
    let title: String
    let body: String
    let date: String
    let ownerInfo: User
    
    private enum CodingKeys: String, CodingKey{
        case url = "html_url"
        case title = "title"
        case body = "body"
        case date = "created_at"
        case ownerInfo = "user"
    }
}
