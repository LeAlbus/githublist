//
//  Project.swift
//  GitHubList
//
//  Created by Pedro Lopes on 14/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation

struct ProjectList: Decodable{
    
    let list: [Project]
    
    private enum CodingKeys: String, CodingKey{
        case list = "items"
    }
}

struct Project: Decodable{
    
    let forksCount: Int
    let starsCount: Int
    let repoName: String
    let repoDescription: String
    let ownerInfo: User
    
    private enum CodingKeys: String, CodingKey{
        case forksCount = "forks_count"
        case starsCount = "stargazers_count"
        case repoName = "name"
        case repoDescription = "description"
        case ownerInfo = "owner"
    }
}
