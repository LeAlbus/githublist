//
//  PullRequestListManager.swift
//  GitHubList
//
//  Created by Pedro Lopes on 14/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import Alamofire

class PullRequestListManager{
    
    static let sharedInstance = PullRequestListManager()
    var pullRequestsRetrieved: [PullRequest] = []
    var repositorySelected: String?
    
    func newRepositorySelected(_ repository: String){
        
        self.pullRequestsRetrieved.removeAll()
        self.repositorySelected = repository
        
    }
    
    func requestPullRequestList(resultPage: Int = 1, successHandler: @escaping (_ successObject: [PullRequest]?) -> ()){
        
        if self.repositorySelected == nil{
            
            successHandler(nil)
            
        } else {
            
            Alamofire.request("\(vBaseURL+vRepositoryEndpoint)\(self.repositorySelected!)\(vPullRequestEndpoint)").responseJSON { response in
                switch response.result {
                case .success:
                    do{
                        let data = response.data
            
                        let responseList = try JSONDecoder().decode([PullRequest].self, from: data!)
                        
                        self.pullRequestsRetrieved.append(contentsOf: responseList)
                        
                        successHandler(responseList)
                       
                    } catch let error{
                        print ("Error while parsing response")
                        print (error)
                        
                    }
                case .failure(_):
                    print ("Could not retrieve information from url")
                    successHandler(nil)
                    
                }
            }
        }
    }
}
