//
//  APIManager.swift
//  GitHubList
//
//  Created by Pedro Lopes on 14/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import Alamofire

class ProjectListManager{
    
    static let sharedInstance = ProjectListManager()
    var projectsRetrieved: [Project] = []
    var isGettingData = false
    
    func requestProjectList(resultPage: Int = 1, successHandler: @escaping (_ successObject: ProjectList?) -> ()){
    
        self.isGettingData = true
        
        Alamofire.request("\(vBaseURL+vRepoListEndpoint)\(vLanguageJava)&\(vResultPage)\(resultPage)").responseJSON { response in
            switch response.result {
            case .success:
                do{
                    let data = response.data
                    let responseList = try JSONDecoder().decode(ProjectList.self, from: data!)

                    self.projectsRetrieved.append(contentsOf: responseList.list)
                    
                    successHandler(responseList)
                    self.isGettingData = false


                } catch let error{
                    print ("Error while parsing response")
                    print (error)
                    self.isGettingData = false

                }
            case .failure(_):
                print ("Could not retrieve information from url")
                successHandler(nil)
                self.isGettingData = false

            }
        }
    }
}
