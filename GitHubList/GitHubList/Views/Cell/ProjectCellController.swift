//
//  ProjectCellController.swift
//  GitHubList
//
//  Created by Pedro Lopes on 13/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class ProjectCellController: UITableViewCell{
    
    @IBOutlet weak var branchesLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var projectDesctiption: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    
    var projectInfo: Project?
        
    func setup(){
        
        if projectInfo != nil{
            
            self.branchesLabel.text = String(projectInfo!.forksCount)
            self.starsLabel.text = String(projectInfo!.starsCount)
            self.projectName.text = projectInfo?.repoName ?? "Project name"
            self.projectDesctiption.text = projectInfo?.repoDescription ?? "Repository description"
            
            self.username.text = projectInfo?.ownerInfo.userName ?? "Repository Owner"
            
            let imagePath = projectInfo?.ownerInfo.avatarPath
            let placeholderImage = UIImage(named: "Profile")!
            self.profilePic.af_setImage(withURL: URL(string: imagePath!)!, placeholderImage: placeholderImage)
        }
    }
}
