//
//  PullRequestCellController.swift
//  GitHubList
//
//  Created by Pedro Lopes on 14/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class PullRequestCellController: UITableViewCell{
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    var pullRequestInfo: PullRequest?
    
    func setup(){
        
        if pullRequestInfo != nil{
            
            self.title.text = pullRequestInfo?.title
            self.body.text = pullRequestInfo?.body ?? "No description provided"
            self.userName.text = pullRequestInfo?.ownerInfo.userName
            
            let dateStr = pullRequestInfo?.date
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let date = dateFormatter.date(from: dateStr!)
            
            dateFormatter.dateFormat = "dd-MMM-yyyy"

            let finalDateString = dateFormatter.string(from: date!)
            self.dateLabel.text = finalDateString
            
            let imagePath = pullRequestInfo?.ownerInfo.avatarPath
            let placeholderImage = UIImage(named: "Profile")!
            self.profileImage.af_setImage(withURL: URL(string: imagePath!)!, placeholderImage: placeholderImage)
        }
    }
}
