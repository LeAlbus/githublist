//
//  ProjectListController.swift
//  GitHubList
//
//  Created by Pedro Lopes on 13/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import UIKit

class ProjectListController: UITableViewController{
    
    var loadedPages = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ProjectListManager.sharedInstance.requestProjectList(){
            response in
            if response != nil{
                self.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProjectListManager.sharedInstance.projectsRetrieved.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectCell")! as! ProjectCellController
        
        if !ProjectListManager.sharedInstance.projectsRetrieved.isEmpty{
            
            let projectForCell = ProjectListManager.sharedInstance.projectsRetrieved[indexPath.row]
            cell.projectInfo = projectForCell
            cell.setup()

        }
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ProjectCellController
        let repositorySelected: String = "\(cell.projectInfo?.ownerInfo.userName as! String)/\(cell.projectInfo?.repoName as! String)"
        PullRequestListManager.sharedInstance.newRepositorySelected(repositorySelected)
        self.performSegue(withIdentifier: "showPullRequests", sender: self)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        
        if ((scrollOffset + scrollViewHeight) - 20 >= scrollContentSizeHeight && !ProjectListManager.sharedInstance.isGettingData)
        {
            self.loadedPages += 1
            ProjectListManager.sharedInstance.requestProjectList(resultPage: self.loadedPages){ response in
                
                self.tableView.reloadData()
            }
        }
    }
}
