//
//  PullRequestListController.swift
//  GitHubList
//
//  Created by Pedro Lopes on 14/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import UIKit

class PullRequestListController: UITableViewController{

    var loadedPages = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PullRequestListManager.sharedInstance.requestPullRequestList(){
            response in
            if response != nil{
                self.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PullRequestListManager.sharedInstance.pullRequestsRetrieved.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestCell")! as! PullRequestCellController
        if !PullRequestListManager.sharedInstance.pullRequestsRetrieved.isEmpty{
            
            let pullRequestForCell = PullRequestListManager.sharedInstance.pullRequestsRetrieved[indexPath.row]
            cell.pullRequestInfo = pullRequestForCell
            cell.setup()
            
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)

        let cell = tableView.cellForRow(at: indexPath) as! PullRequestCellController
        
        if let url = cell.pullRequestInfo?.url{
            UIApplication.shared.open(URL(string : url)!, options: [:], completionHandler: { (status) in
            })
        }
    }
    
}
