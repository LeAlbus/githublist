//
//  Constants.swift
//  GitHubList
//
//  Created by Pedro Lopes on 14/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation

//Endpoints
let vLanguageJava = "language:java"
let vLanguageSwift = "language:swift"
let vLanguageObjC = "language:objectivec"

let vResultPage = "page="

let vBaseURL = "https://api.github.com"
let vRepoListEndpoint = "/search/repositories?q="
let vRepositoryEndpoint = "/repos/"
let vPullRequestEndpoint = "/pulls"


